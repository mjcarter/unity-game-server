package state

import lm "unity-game-server/pkg/locomotion"

type State interface {
	SetTransform(transform *lm.PlayerTransform) error
	GetTransform(payload *lm.PlayerPayload) (*lm.Transform, error)
	RequestID(request *lm.IDRequest) (*lm.IDResponse, error)
	PollPlayerList(request *lm.PlayerListRequest) (*lm.PlayerListResponse, error)
}
