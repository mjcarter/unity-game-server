package redis

import (
	"fmt"
	r "github.com/gomodule/redigo/redis"
	"log"
	"math/rand"
	"reflect"
	"strconv"
	"time"
	st "unity-game-server/internal/state"
	lm "unity-game-server/pkg/locomotion"
)

type state struct {
	pool *r.Pool
}

func (s state) SetTransform(transform *lm.PlayerTransform) error {
	connection := s.pool.Get()
	defer connection.Close()

	var args []interface{}
	//TODO: Check if it's more efficient to break out transform.Transform into a new variable
	args = append(
		args, fmt.Sprintf("player:%v", transform.Player),
		"LocX", transform.Transform.LocX,
		"LocY", transform.Transform.LocY,
		"LocZ", transform.Transform.LocZ,
		"RotX", transform.Transform.RotX,
		"RotY", transform.Transform.RotY,
		"RotZ", transform.Transform.RotZ,
		"RotW", transform.Transform.RotW,
	)
	_, err := connection.Do("hmset", args...)
	return err
}

func (s state) GetTransform(payload *lm.PlayerPayload) (*lm.Transform, error) {
	connection := s.pool.Get()
	defer connection.Close()

	var args []interface{}
	//TODO: Check if it's more efficient to break out transform.Transform into a new variable
	args = append(
		args, fmt.Sprintf("player:%v", payload.Id), "LocX", "LocY", "LocZ", "RotX", "RotY", "RotZ", "RotW",
	)
	reply, err := connection.Do("hmget", args...)
	if err != nil {
		log.Fatalf("Bad stuff: %v", err)
		return nil, err
	}
	v, err := r.Float64s(reply, err)

	return &lm.Transform{
		LocX: v[0],
		LocY: v[1],
		LocZ: v[2],
		RotX: v[3],
		RotY: v[4],
		RotZ: v[5],
		RotW: v[6],
	}, nil
}

func (s state) RequestID(request *lm.IDRequest) (*lm.IDResponse, error) {
	connection := s.pool.Get()
	defer connection.Close()

	newID := rand.Int31()
	var args []interface{}
	//TODO: Check if it's more efficient to break out transform.Transform into a new variable
	args = append(
		args, fmt.Sprintf("player:%v", newID),
		"LocX", 0,
		"LocY", 0,
		"LocZ", 0,
		"RotX", 0,
		"RotY", 0,
		"RotZ", 0,
		"RotW", 0,
	)
	_, err := connection.Do("hmset", args...)
	if err != nil {
		return nil, err
	}
	return &lm.IDResponse{Id: newID}, nil
}

func (s state) PollPlayerList(request *lm.PlayerListRequest) (*lm.PlayerListResponse, error) {
	connection := s.pool.Get()
	defer connection.Close()

	var args []interface{}
	args = append(args, "0", "match", "player:*")

	reply, err := connection.Do("scan", args...)
	if err != nil {
		return nil, err
	}
	values, err := r.Values(reply, err)
	if err != nil {
		return nil, err
	}
	players, err := r.Strings(values[1], err)
	p := make(map[int32]*lm.Player, 0)
	for i, v := range players {
		m, _ := strconv.Atoi(v[7:])
		p[int32(i)] = &lm.Player{Id: int32(m), Username: v}
	}

	return &lm.PlayerListResponse{
		PlayerList: p,
	}, nil
}

func NewState() (st.State, error) {
	p := r.Pool{
		MaxIdle:     3,
		IdleTimeout: 248 * time.Second,
		Dial:        func() (r.Conn, error) { return r.Dial("tcp", ":6379") },
	}

	newState := state{
		pool: &p,
	}
	v := reflect.ValueOf(&newState).Elem()
	iState := v.Interface().(st.State)
	return iState, nil
}
