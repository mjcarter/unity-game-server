package local

import (
	"reflect"
	st "unity-game-server/internal/state"
	lm "unity-game-server/pkg/locomotion"
)

type state struct {
	player map[int32]lm.Transform
}

func (s state) SetTransform(transform *lm.PlayerTransform) error {
	s.player[transform.Player] = *transform.Transform
	return nil
}

func (s state) GetTransform(payload *lm.PlayerPayload) (*lm.Transform, error) {
	v, ok := s.player[payload.Id]
	if ok {
		return &v, nil
	}

	return &lm.Transform{
		LocX: 0,
		LocY: 0,
		LocZ: 0,
		RotX: 0,
		RotY: 0,
		RotZ: 0,
		RotW: 0,
	}, nil
}

func NewState() (st.State, error) {
	newState := state{
		player: make(map[int32]lm.Transform),
	}
	v := reflect.ValueOf(&newState).Elem()
	iState := v.Interface().(st.State)
	return iState, nil
}
