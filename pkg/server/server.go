package server

import (
	"context"
	"unity-game-server/internal/state"
	lm "unity-game-server/pkg/locomotion"
)

type Server struct {
	Backend state.State
}

func (s Server) PollTransform(ctx context.Context, curr *lm.PlayerPayload) (*lm.Transform, error) {
	return s.Backend.GetTransform(curr)
}

func (s Server) PushTransform(ctx context.Context, curr *lm.PlayerTransform) (*lm.Basic, error) {
	err := s.Backend.SetTransform(curr)
	if err != nil {
		return &lm.Basic{Response: "BAD"}, err
	}
	return &lm.Basic{Response: "OK"}, nil
}

func (s Server) RequestID(ctx context.Context, request *lm.IDRequest) (*lm.IDResponse, error) {
	return s.Backend.RequestID(request)
}

func (s Server) PollPlayerList(ctx context.Context, request *lm.PlayerListRequest) (*lm.PlayerListResponse, error) {
	return s.Backend.PollPlayerList(request)
}
