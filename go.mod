module unity-game-server

go 1.14

require (
	github.com/golang/protobuf v1.4.2
	github.com/gomodule/redigo v1.8.2
	google.golang.org/grpc v1.31.0
	google.golang.org/protobuf v1.25.0
)
