package main

import (
	_ "fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	re "unity-game-server/internal/state/redis"
	lm "unity-game-server/pkg/locomotion"
	srv "unity-game-server/pkg/server"
)

const (
	port       = ":50051"
	colorBytes = 3
)

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Failed to listen on port [%s]: %v", port, err)
	}
	s := grpc.NewServer()
	backend, err := re.NewState()
	if err != nil {
		log.Fatalf("Failed to create state backend: %v", err)
	}
	lm.RegisterLocomotionServiceServer(s, &srv.Server{
		Backend: backend,
	})
	log.Println("Starting server...")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to start the server: %v", err)
	}
}
