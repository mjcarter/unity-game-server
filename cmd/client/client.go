package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"log"
	lm "unity-game-server/pkg/locomotion"
)

const (
	port       = ":50051"
	colorBytes = 3
)

func main() {
	//lis, err := net.Listen("tcp", port)
	//if err != nil {
	//	log.Fatalf("Failed to listen on port [%s]: %v", port, err)
	//}
	//s := grpc.NewServer()
	//lm.RegisterLocomotionServiceServer(s, &server{})
	//log.Printf("Starting server...")
	//if err := s.Serve(lis); err != nil {
	//	log.Fatalf("Failed to start the server: %v", err)
	//}

	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Dial failed: %v", err)
	}
	defer conn.Close()

	client := lm.NewLocomotionServiceClient(conn)

	v, err := client.PollTransform(context.Background(), &lm.PlayerPayload{Id: 1})
	if err != nil {
		log.Fatalf("Failed to poll: %v", err)
	}
	fmt.Println(v.LocX)
	//player_loc, err := client.GetLocationUpdate(context.Background(), &lm.Player{Id: 1})
	//if err != nil {
	//	log.Fatalf("Request failed: %v", err)
	//}
	//id, x, y := player_loc.Player.Id, player_loc.PlayerLocation.X, player_loc.PlayerLocation.Y
	//log.Printf("Player %v is located at %v, %v", id, x, y)
	//
	//client.UpdateLocation(context.Background(), &lm.PlayerLocation{
	//	Player:         &lm.Player{Id: 1},
	//	PlayerLocation: &lm.Location{X: 5, Y: 25},
	//})
	//
	//player_loc, err = client.GetLocationUpdate(context.Background(), &lm.Player{Id: 1})
	//if err != nil {
	//	log.Fatalf("Request failed: %v", err)
	//}
	//id, x, y = player_loc.Player.Id, player_loc.PlayerLocation.X, player_loc.PlayerLocation.Y
	//log.Printf("Player %v is located at %v, %v", id, x, y)
	//c, err := r.Dial("tcp", ":6379")
	//if err != nil {
	//	log.Fatalf("Failed to connect to redis server: %T %v", err,err)
	//}
	//
	//_, err = c.Do("set", "a", "b")
	//if err !=nil {
	//	log.Fatalf("Failed to run command: %T %v", err, err)
	//}
}
